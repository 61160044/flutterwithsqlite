import 'package:flutter/widgets.dart';
import 'dog.dart';
import 'dog_dao.dart';

void main() async {

  var fido = Dog(
    id: 0,
    name: 'Fido',
    age: 35,
  );
  var dido = Dog(
    id: 1,
    name: 'Dido',
    age: 20,
  );
  await DogDAO.insertDog(fido);
  await DogDAO.insertDog(dido);

  print(await DogDAO.dogs());

  fido = Dog(id: fido.id, name: fido.name, age: fido.age + 7);
  await DogDAO.updateDog(fido);
  print(await DogDAO.dogs());

  await DogDAO.deleteDog(0);
  print(await DogDAO.dogs());
}
